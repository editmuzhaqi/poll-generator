require('dotenv').config();
const express = require('express');
const session = require('cookie-session');
const bodyParser = require('body-parser');
const path = require('path');

const routes = require('./routes/index');

const app = express();

app.use(session({
    secret: process.env.SECRET,
    resave: false,
    saveUninitialized: true,
    expires: new Date(Date.now() + 60 * 60 * 1000 * 2),
    cookie: { secure: false }
  }))
  
app.use(express.static('public'));
app.use(express.json());
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(bodyParser.urlencoded({ extended: true }));
app.use('/', routes);

module.exports = app;