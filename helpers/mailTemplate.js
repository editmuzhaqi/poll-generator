  const template = (name, token, poll) => {
    return `<!doctype html>
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml"
      xmlns:o="urn:schemas-microsoft-com:office:office">
    
    <head>
      <title>Poll Invitation</title>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta name="viewport" content="width=device-width,initial-scale=1">
      <style type="text/css">
        #outlook a {
          padding: 0;
        }
    
        body {
          margin: 0;
          padding: 0;
          -webkit-text-size-adjust: 100%;
          -ms-text-size-adjust: 100%;
        }
    
        table,
        td {
          border-collapse: collapse;
        }
    
        img {
          border: 0;
          height: auto;
          line-height: 100%;
          outline: none;
          text-decoration: none;
          -ms-interpolation-mode: bicubic;
        }
    
        p {
          display: block;
          margin: 13px 0;
        }
      </style>
      <style type="text/css">
        @media only screen and (min-width:480px) {
          .mj-column-per-100 {
            width: 100% !important;
            max-width: 100%;
          }
        }
      </style>
      <style media="screen and (min-width:480px)">
        .moz-text-html .mj-column-per-100 {
          width: 100% !important;
          max-width: 100%;
        }
      </style>
      <style type="text/css">
        [owa] .mj-column-per-100 {
          width: 100% !important;
          max-width: 100%;
        }
      </style>
      <style type="text/css">
        @media only screen and (max-width:480px) {
          table.mj-full-width-mobile {
            width: 100% !important;
          }
    
          td.mj-full-width-mobile {
            width: auto !important;
          }
        }
      </style>
    </head>
    
    <body style="word-spacing:normal;background-color:#F4F4F4;">
      <div style="background-color:#F4F4F4;">
        <div
          style="background:transparent url('http://go.mailjet.com/tplimg/mtrq/b/ox8s/mg1qn.png') center top / auto repeat;background-position:center top;background-repeat:repeat;background-size:auto;margin:0px auto;max-width:600px;">
          <div style="line-height:0;font-size:0;">
            <table align="center" background="http://go.mailjet.com/tplimg/mtrq/b/ox8s/mg1qn.png" border="0" cellpadding="0"
              cellspacing="0" role="presentation"
              style="background:transparent url('http://go.mailjet.com/tplimg/mtrq/b/ox8s/mg1qn.png') center top / auto repeat;background-position:center top;background-repeat:repeat;background-size:auto;width:100%;">
              <tbody>
                <tr>
                  <td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;">
                    <div class="mj-column-per-100 mj-outlook-group-fix"
                      style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                      <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;"
                        width="100%">
                        <tbody>
                          <tr>
                            <td align="center" vertical-align="top"
                              style="font-size:0px;padding:10px 25px;word-break:break-word;">
                              <table border="0" cellpadding="0" cellspacing="0" role="presentation"
                                style="border-collapse:collapse;border-spacing:0px;">
                                <tbody>
                                  <tr>
                                    <td style="width:200px;"><img alt="" height="auto"
                                        src="http://go.mailjet.com/tplimg/mtrq/b/ox8s/mg1qn.png"
                                        style="border:none;border-radius:px;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;"
                                        width="200"></td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div style="background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:600px;">
          <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"
            style="background:#ffffff;background-color:#ffffff;width:100%;">
            <tbody>
              <tr>
                <td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;">
                  <div class="mj-column-per-100 mj-outlook-group-fix"
                    style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;"
                      width="100%">
                      <tbody>
                        <tr>
                          <td align="left" vertical-align="top"
                            style="font-size:0px;padding:10px 25px;padding-top:0px;padding-bottom:0px;word-break:break-word;">
                            <div
                              style="font-family:Arial, sans-serif;font-size:13px;letter-spacing:normal;line-height:1;text-align:left;color:#000000;">
                              <p class="text-build-content" data-testid="fpvOWxh7Lh3"
                                style="margin: 10px 0; margin-top: 10px;"><span
                                  style="color:#5e6977;font-family:Arial;font-size:18px;line-height:20px;">Dear
                                  ${name},</span></p>
                              <p class="text-build-content" data-testid="fpvOWxh7Lh3"
                                style="margin: 10px 0; margin-bottom: 10px;"><span
                                  style="color:#5e6977;font-family:Arial;font-size:18px;line-height:20px;">You've been
                                  invited to vote on the poll ${poll}&nbsp;</span></p>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td align="center" vertical-align="top"
                            style="font-size:0px;padding:15px 30px;word-break:break-word;">
                            <table border="0" cellpadding="0" cellspacing="0" role="presentation"
                              style="border-collapse:separate;line-height:100%;">
                              <tbody>
                                <tr>
                                  <td align="center" bgcolor="#41B8FF" role="presentation"
                                    style="border:none;border-radius:0px;cursor:auto;background:#41B8FF;"
                                    valign="top"><a href="http://localhost:3000/vote/${token}"
                                      style="display:inline-block;background:#41B8FF;color:#ffffff;font-family:Arial, sans-serif;font-size:13px;font-weight:normal;line-height:120%;margin:0;text-decoration:none;text-transform:none;padding:10px 25px;border-radius:0px;"
                                      target="_blank"><span
                                        style="background-color:#41B8FF;color:#ffffff;font-family:Arial;font-size:13px;"><b>VOTE
                                          HERE</b></span></a></td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </body>
    
    </html>`
  }

exports.template = template;