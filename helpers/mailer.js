require('dotenv').config();
const nodemailer = require('nodemailer');
const { template } = require('./mailTemplate.js');

const invitationMail = (mailList) => {

    let transporter = nodemailer.createTransport({
        service: process.env.MAIL_SERVICE,
        host: process.env.MAIL_HOST,
        logger: true,
        auth: {
            user: process.env.MAIL_USERNAME,
            pass: process.env.MAIL_PASSWORD
        }
    });

    mailList.forEach(receiver => {
        // TODO: test for large input
        let html = template(receiver.receiverName, receiver.receiverToken, receiver.pollTitle);

        let mailOptions = {
            from: process.env.MAIL_USERNAME,
            to: receiver.receiverEmail,
            subject: 'Invitation to vote on company poll',
            html: html
        };

        transporter.sendMail(mailOptions, function (err, data) {
            if (err) {
                console.log("Error " + err);
            } else {
                console.log("Email sent successfully");
            }
        });

    });
    
}

exports.invitationMail = invitationMail;