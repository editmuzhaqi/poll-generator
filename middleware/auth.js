require('dotenv').config();
const jwt = require("jsonwebtoken");
const mongoose = require('mongoose');
const Token = mongoose.model('Token');
const User = mongoose.model('User');


const auth = (req, res, next) => {
  let token;
  const excludedRoutes = ['/login', '/register', '/'];

  if (excludedRoutes.includes(req.path)) {
    return next();
  }
  if (req.session.token || req.headers.authorization) {
    token = req.session.token || req.headers.authorization.split(' ')[1];
  }
  if (!token) {
    return res.status(403).send("A token is required for authentication");
  }
  try {
    jwt.verify(token, process.env.SECRET);
  } catch (err) {
    return res.status(401).send("Invalid Token");
  }
  return next();
};


const hasVoted = async (req, res, next) => {
  let vote;

  if (req.params.token) {
    vote = req.params.token;
  }
  try {
    let found = await Token.findOne({'jwt': vote});
    let user = req.session.token || req.headers.authorization.split(' ')[1];
    let voter = jwt.verify(user, process.env.SECRET).user_id;
    if (!found || found.user != voter) {
      return res.status(403).send("Poll you're trying to vote doesn't exist");
    }
    if (found.consumed) {
      return res.status(403).send("You've already voted on this poll");
    }
  } catch (err) {
    return res.status(401).send("No poll found");
  }
  return next();
};


const isAdmin = async (req, res, next) => {
  let token;
  if (req.session.token || req.headers.authorization) {
    token = req.session.token || req.headers.authorization.split(' ')[1];
  }
  if (!token) {
    return res.status(403).send("Please login");
  }
  try {
    let decoded = jwt.verify(token, process.env.SECRET).user_id;
    let user = await User.findById(decoded);
    if (!(user.admin || req.params.id === decoded)) {
      return res.status(401).send("Unauthorized");
    }
  } catch (err) {
    return res.status(401).send("Invalid Token");
  }
  return next();
};

exports.auth = auth;
exports.hasVoted = hasVoted;
exports.isAdmin = isAdmin;