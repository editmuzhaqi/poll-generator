const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const AnswerSchema = new Schema({
  poll: {
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'Poll',
    required: true,
  },
  user: {
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'User',
    required: false,
    default: null,
  },
  answers_desc: [
      {
          lineup: {
              type: Number,
              required: true,
          },
          answer: {
              type: String,
              required: true,
          },
      }
  ],
  answers_rate: [
      {
          lineup: {
              type: Number,
              required: true,
          },
          answer: {
              type: Number,
              required: true,
              min: 1,
              max: 10,
          },
      }
  ],
},
{
    timestamps: true
});


module.exports = mongoose.model('Answer', AnswerSchema);