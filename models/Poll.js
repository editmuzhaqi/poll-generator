const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const PollSchema = new Schema({
  title: {
    type: String,
    required: true,
    trim: true,
  },
  creator: {
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'User',
    required: true,
  },
  anon: {
    type: Boolean,
    default: false,
  },
  questions_desc: [
      {
          lineup: {
              type: Number,
              required: true,
          },
          question: {
              type: String,
              required: true,
          },
      }
  ],
  questions_rate: [
      {
          lineup: {
              type: Number,
              required: true,
          },
          question: {
              type: String,
              required: true,
          },
      }
  ],
},
{
    timestamps: true
});


module.exports = mongoose.model('Poll', PollSchema);