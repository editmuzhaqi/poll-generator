const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const TokenSchema = new Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'User',
    required: false,
    default: null,
  },
  poll: {
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'Poll',
    required: true,
  },
  jwt: {
    type: String,
    required: true,
    unique: true,
  },
  consumed: {
    type: Boolean,
    default: false
  }
},
{
  timestamps: true
});


module.exports = mongoose.model('Token', TokenSchema);