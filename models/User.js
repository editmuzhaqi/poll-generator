const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const UserSchema = new Schema({
  name: {
    type: String,
    required: true,
    trim: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
    trim: true,
  },
  password: {
      type: String,
      required: true,
  },
  admin: {
      type: Boolean,
      default: false,
  }
},
{
  timestamps: true
});


module.exports = mongoose.model('User', UserSchema);