require('dotenv').config();
const express = require('express');
const router = express.Router();
const { validationResult } = require('express-validator');
const Token = require('../models/Token');
const User = require('../models/User');
const Answer = require('../models/Answer');
const Poll = require('../models/Poll');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { auth, hasVoted, isAdmin } = require('../middleware/auth.js');
const { validatePoll, validateAnswer } = require('../validators/poll.js');
const { validateRegister, validateLogin } = require('../validators/user.js');
const { invitationMail } = require('../helpers/mailer.js');

router.use(auth);

// TODO: Set all in separate routes and controllers
// TODO: Redirect views

router.get('/', async(req,res) => {
  let token;
  if (!req.session.token && !req.headers.authorization) {
    res.render('welcome', {title: 'Welcome'});
  }
  token = req.session.token || req.headers.authorization.split(' ')[1];
  let decoded = jwt.verify(token, process.env.SECRET).user_id;
  let user = await User.findById(decoded);
  if (!user) {
    res.render('welcome', {title: 'Welcome'});
  }
  if (user.admin) {
    res.redirect('/users')
  }
  res.redirect(`/users/${decoded}`)
})

router.get('/add', (req,res) => {
  res.render('createPoll', {title: 'Create a Poll'});
})

router.post('/login', validateLogin, async(req, res) => {
    
  try {
    let errors = validationResult(req);
    if (errors.isEmpty()) {
      let { email } = req.body;
      let user = await User.findOne({ email }).select("-password");

      let token = jwt.sign(
        { user_id: user._id },
        process.env.SECRET,
        { expiresIn: "2h" }
      );

      req.session.token = token;
      if (user.admin) {
        res.redirect('/users')
      }
      res.redirect(`/users/${user._id}`)
    } else {
      res.render('welcome', {title: 'Welcome', errors: errors.array(), data: req.body});
    }
  } catch (err) {
      res.render('err', {title: 'ERROR', error:{message: err, code:400}});
      console.log(err);
  }
});

router.post('/register', validateRegister, async(req, res) => {
    
  try {
    let errors = validationResult(req);
    console.log(errors)
    if (errors.isEmpty()) {
        
      let { name, email, password } = req.body;
      encryptedPassword = await bcrypt.hash(password, 10);

      let user = await User.create({
        name,
        email: email.toLowerCase(),
        password: encryptedPassword,
      });

      // Create token
      let token = jwt.sign(
        { user_id: user._id },
        process.env.SECRET,
        { expiresIn: "2h"},
      );
      
      req.session.token = token;
      res.redirect(`/users/${user._id}`)
    } else {
      res.render('welcome', {title: 'Welcome', errors: errors.array(), data: req.body});
    }
  } catch (err) {
      res.render('err', {title: 'ERROR', error:{message: err, code:400}});
      console.log(err);
}
});

router.get('/users', isAdmin, (req,res) => {
    User.find().select("-password")
    .then((users) => {
      res.render('users', {title: 'Users Page', users});
    })
    .catch((err) => { res.render('err', {title: 'ERROR', error:{message: err, code:400}}) });
})
// only for admin

router.get('/users/:id', isAdmin, (req,res) => {
    User.findById(req.params.id)
    .then(user => {
      Answer.find({user: user._id}).populate('poll')
      .then(answers => {
        answers.forEach(answer => {

          let cards_q = [];
          let q1 = answer.poll.questions_desc, q2 = answer.poll.questions_rate
          cards_q = q1.concat(q2)
          cards_q.sort(function(a, b) {
            return a.lineup - b.lineup;
          });
          answer.cards_q = cards_q;
          let cards_a = [];
          let a1 = answer.answers_desc, a2 = answer.answers_rate
          cards_a = a1.concat(a2)
          cards_a.sort(function(a, b) {
            return a.lineup - b.lineup;
          });
          answer.cards_a = cards_a;
        })
        
        let result = {
          "name": user.name,
          "email": user.email,
          "admin": user.admin,
          "_id": user._id,
          answers
        }
        res.render('user', {title: 'User Page', result});
      })
    })
    .catch((err) => { res.render('err', {title: 'ERROR', error:{message: 'User does not exist', code:400}}) });
})
// only for admin and own user

router.delete('/users/:id', isAdmin, (req, res) => {
  let token = req.session.token || req.headers.authorization.split(' ')[1];
  let decoded = jwt.verify(token, process.env.SECRET).user_id;
  if (req.params.id === decoded) {
    console.log('same user')
    return res.status(500).send("Cannot delete own user");
  }
  User.findByIdAndDelete(req.params.id)
    .then(user => {
      if (user) {
        res.send('User deleted')
      } else {
        res.send('User does not exist')
      }
    })
    .catch((err) => { res.send('Failed to delete') });
});
// only for admin and own user

router.get('/polls', isAdmin, (req,res) => {
    Poll.find()
    .then((polls) => {
      res.render('polls', {title: 'Polls Page', polls});
    })
    .catch((err) => { res.render('err', {title: 'ERROR', error:{message: err, code:400}}) });
})
// only for admin

router.get('/polls/:id', isAdmin, (req,res) => {
    Poll.findById(req.params.id).populate('creator')
    .then(poll => {
      Answer.find({poll: poll._id})
      .then(answers => {
        let cards_q = [];
        let q1 = poll.questions_desc, q2 = poll.questions_rate
        cards_q = q1.concat(q2)
        cards_q.sort(function(a, b) {
          return a.lineup - b.lineup;
        });
        answers.forEach(answer => {
          let cards_a = [];
          let a1 = answer.answers_desc, a2 = answer.answers_rate
          cards_a = a1.concat(a2)
          cards_a.sort(function(a, b) {
            return a.lineup - b.lineup;
          });
          answer.cards_a = cards_a;
        })
        let result = {
            title: poll.title,
            creator: poll.creator.name,
            created_at: new Date(poll.createdAt),
            questions: cards_q,
            answers: answers
        }
        res.render('poll', {title: 'Poll Page', result});
      })
    })
    .catch((err) => { res.render('err', {title: 'ERROR', error:{message: 'Poll does not exist', code:400}}) });
})
// only for admin

router.post('/polls', [ isAdmin, validatePoll ], async(req,res) => {
    
  try {
    let errors = validationResult(req);
    if (errors.isEmpty()) {
        
      let { title, anon, questions_desc, questions_rate } = req.body;
      let token = req.session.token || req.headers.authorization.split(' ')[1];
      let creator = jwt.verify(token, process.env.SECRET).user_id;
      let poll = await Poll.create({
        title,
        creator,
        anon,
        questions_desc,
        questions_rate
      });
      
      let receivers = await User.find();
      let mailList = [];
      receivers.forEach(receiver => {
          let receiverEmail = receiver.email
          let receiverToken = jwt.sign({user_id: receiver._id, poll_id: poll._id }, process.env.SECRET)
          mailList.push({receiverEmail: receiverEmail, receiverToken: receiverToken, receiverName: receiver.name, pollTitle: poll.title});
          Token.create({
            "user": receiver._id,
            "poll": poll._id,
            "jwt": receiverToken
          })
      });
      
      invitationMail(mailList);
      res.send('Poll created successfully')
    } else {
      console.log(errors);
      res.render('createPoll', {title: 'Create Poll Page', errors: errors.array(), data: req.body});
    }
  } catch (err) {
      res.render('err', {title: 'ERROR', error:{message: err, code:400}});
      console.log(err);
  }
})
// only for admin

router.delete('/polls/:id', isAdmin, (req, res) => {
  Poll.findByIdAndDelete(req.params.id)
    .then(poll => {
      if (poll) {
        res.send('Poll deleted')
      } else {
        res.send('Poll does not exist')
      }
    })
    .catch((err) => { res.send('Failed to delete') });
});
// only for admin and own user

router.get('/vote/:token', hasVoted, async(req,res) => {
    try {
        let poll_id = jwt.verify(req.params.token, process.env.SECRET).poll_id;
        let poll = await Poll.findById(poll_id);
        res.render('vote', {title: 'Vote Page', poll, token: req.params.token});
    } catch (err) {
        console.log(err);
        res.render('err', {title: 'ERROR', error:{message: "Poll doesn't exist", code:400}});
    }
})
// middleware to check if has voted before

router.post('/vote/:token', [ hasVoted, validateAnswer ], async(req,res) => {
      
  try {
    let errors = validationResult(req);
    if (errors.isEmpty()) {
      let poll_id = jwt.verify(req.params.token, process.env.SECRET).poll_id;
      let user_id;
      // TODO: turn into promises instead of callback hell
      await Poll.findById(poll_id)
      .then(async(poll) => {
        if (poll.anon) user_id = null;
        else user_id = jwt.verify(req.params.token, process.env.SECRET).user_id;

        await Answer.create({
            poll: poll_id,
            user: user_id,
            answers_desc: req.body.answers_desc,
            answers_rate: req.body.answers_rate
        })
        .then(async (answer) => {
            await Token.findOneAndUpdate({'jwt': req.params.token}, {'consumed': true}).then(token => res.send('Vote saved'))
        })
      })
      .catch(err => {
        console.log(err);
        res.send(err);
      })
    } else {
      res.send(errors);
    }
  } catch (err) {
    console.log(err);
    res.render('err', {title: 'ERROR', error:{message: err, code:400}});
  }
})
// middleware to check if has voted before

router.post('/logout', (req,res) => {
    req.session.destroy();
    res.redirect('/')
})
module.exports = router;