const { check } = require('express-validator');
const Poll = require('../models/Poll');
const jwt = require('jsonwebtoken');
require('dotenv').config();

// TODO: add error messages to constants
const validatePoll = [
        check('title').isLength({ min: 3 }).withMessage('Title should be at least 3 characters long'),
        check('anon').isBoolean().optional().withMessage('Anonimity should be a boolean value'),
        check('questions_desc').isArray().custom((value, { req }) => {
            if (!value.length && !req.body.questions_rate.length) {
              throw new Error('Your poll must contain at least one question');
            }
            return true;
        }),
        check('questions_rate').isArray().optional(),
        check('questions_desc.*.lineup').exists().isInt(),
        check('questions_desc.*.question').exists().isString().isLength({ min: 4 }).withMessage('Question should contain at least 4 characters.'),
        check('questions_rate.*.lineup').exists().isInt(),
        check('questions_rate.*.question').exists().isString().isLength({ min: 4 }).withMessage('Question should contain at least 4 characters.'),
    ]

const validateAnswer = [
  // TODO: add a check on whether answers correspond to questions of the poll by lineup

      check('answers_desc').isArray().custom(async(value, { req }) => {
          if (value[0] == '')value = [];
          if (req.body.answers_rate[0] == '')req.body.answers_rate = [];
          if (!value.length && !req.body.answers_rate.length) {
            throw new Error('You have not answered any questions');
          }
          let poll_id = jwt.verify(req.params.token, process.env.SECRET).poll_id;
          let poll = await Poll.findById(poll_id)
          
          if (value.length !== poll.questions_desc.length || req.body.answers_rate.length !== poll.questions_rate.length) {
            return Promise.reject('You have not supplied an answer for all questions');
          }
          value.map(answer => { 
            let result=poll.questions_desc.filter(question => question.lineup==answer.lineup);
            if(result.length<1) { throw new Error('Answers do not match poll') }
          })
          req.body.answers_rate.map(answer => { 
            let result=poll.questions_rate.filter(question => question.lineup==answer.lineup);
            if(result.length<1) { throw new Error('Answers do not match poll') }
          })
          return true;
      }),
      check('answers_rate').isArray().optional(),
      check('answers_desc.*.lineup').exists().isInt(),
      check('answers_desc.*.answer').exists().isString(),
      check('answers_rate.*.lineup').exists().isInt(),
      check('answers_rate.*.answer').exists().isInt({min:1, max:10}),
  ]

exports.validateAnswer = validateAnswer;
exports.validatePoll = validatePoll;