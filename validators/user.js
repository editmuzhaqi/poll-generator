const { check } = require('express-validator');
const User = require('../models/User');
const bcrypt = require('bcryptjs');

const validateRegister = [
        check('name').isLength({ min: 4 }).withMessage('Name should be at least 4 characters long'),
        check('email').isEmail().normalizeEmail().withMessage('Email is required').bail().custom(async email => {
            return await User.findOne({ email }).then(user => {
              if (user) {
                return Promise.reject('E-mail already in use');
              }
            })
        }),
        check('password').isLength({ min: 6 }).withMessage('Password should be at least 6 characters long'),
        check('password_confirmation').custom((value, { req }) => {
            if (value !== req.body.password) {
              throw new Error('Password confirmation does not match password');
            }
            return true;
        }),
    ]

const validateLogin = [
        check('email').isEmail().normalizeEmail().withMessage('Email is required').bail().custom(async (email, {req}) => {
            return await User.findOne({ email }).then(async user => {
              if (!user) {
                return Promise.reject('E-mail does not exist. Please register first');
              }
              else {
                let pw = req.body.password;
                if(! await bcrypt.compare(pw, user.password)) return Promise.reject('Password incorrect.');
              }
            })
        })
    ]    

exports.validateRegister = validateRegister;
exports.validateLogin = validateLogin;